import unittest
from App import App


class AppTest(unittest.TestCase):

    def test_GetUserInfoURL(self):
        name = "niwasawa"
        app = App(name)
        self.assertEquals("https://api.github.com/users/niwasawa", app.getUserInfoURL())


if __name__ == '__main__':
    unittest.main()
