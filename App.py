import sys
import requests
import json


class App:
    def __init__(self, name):
        self.name = name

    def getUserInfoURL(self):
        return "https://api.github.com/users/" + self.name

    def getUserInfoJSON(self):
        url = self.getUserInfoURL()
        res = requests.get(url)
        return res.text()


if __name__ == "__main__":
    for i, name in enumerate(sys.argv):
        print("name: " + name)
        jsonString = App(name).getUserInfoJSON();
        json_ = json.loads(jsonString);
        print(json.dumps(json_, indent=2));
